import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

# sequential model used for a plain stack of layers where each layer has
# one input tensor and one output tensor

# sequential model behaves like a list of layers. we can add() and pop()
# note that we've only specified one dimension for the dense layer right now -
# the other to be decided when input is given.
model = keras.Sequential([\
                layers.Dense(2, activation='relu', name='layer1'),\
                layers.Dense(3, activation='relu', name='layer2'),\
                layers.Dense(4, name='layer3')],\
                name='my_sequential')

#x = tf.ones((3, 3))
# at this point our weights are uninitialised
#print(model.layers[0].weights)

# now we'll have numbers in the matrix
#y = model(x)
#for layer in model.layers:
#    print(layer.weights)

# applying an individual layer
#print(model.layers[0](x))
#model.summary()

new_model = keras.Sequential()
new_model.add(layers.Dense(2, activation='tanh', input_shape=(4,)))

new_model.summary()
#print(new_model.variables)
#print()

x = tf.ones((5, 4))
#print("applying to 5x4")
#print(new_model.layers[0](x))

x = tf.ones((8, 4))
#print("applying to 8x4")
#print(new_model.layers[0](x))
# what happens after we give input of a different size?
#print("what happens to our layer?")
#print(new_model.variables)

""" example of stacking layers and printing summaries along the way """
model = keras.Sequential()
model.add(keras.Input(shape=(250, 250, 3))) # 250x250 RGB
model.add(layers.Conv2D(32, 5, strides=2, activation='relu'))
model.add(layers.Conv2D(32, 3, activation='relu'))
model.add(layers.MaxPooling2D(3))
model.summary()

""" feature extraction """
initial_model = keras.Sequential([\
                    keras.Input(shape=(250, 250, 3)),
                    layers.Conv2D(32, 5, activation='relu'),
                    layers.Conv2D(32, 3, activation='relu')])
# extract features out of all the layers
feature_extractor = keras.Model(\
                        inputs = initial_model.inputs,
                        outputs = [layer.output for layer in initial_model.layers])
# or we can just extract one
single_feature_extractor = keras.Sequential([\
                        keras.Input(shape=(250, 250, 3)),
                        layers.Conv2D(32, 5, strides=2, activation='relu'),
                        layers.Conv2D(32, 3, activation='relu', name='intermediate_layer'),
                        layers.Conv2D(32, 3, activation='relu')])

# call feature extractor on test input
x = tf.ones((1, 250, 250, 3))
features = feature_extractor(x)
single_feature = single_feature_extractor(x)

""" transfer learning - freezing bottom layers and only training top layers

to freeze layers, we can do

for layer in model.layers[:-1]: # or whatever index we want
    layer.trainable = False
"""

