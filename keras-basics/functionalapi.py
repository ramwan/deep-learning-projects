import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from PIL import Image
import numpy as np

displayImage = False
train = False

# vector of length 784 with no batch size
inputs= keras.Input(shape=(784,))

dense = layers.Dense(64, activation='relu')
x = dense(inputs)
x = layers.Dense(64, activation='relu')(x)
outputs = layers.Dense(10)(x)

model = keras.Model(inputs=inputs,outputs=outputs, name='mnist_model')
#model.summary()
#print(inputs.shape, inputs.dtype)
# this saves our model graph as an image
#keras.utils.plot_model(model, "model.png", show_shapes=True)
''' we note that we couldve done this via keras.Sequential as well '''

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

if displayImage:
    Image.fromarray( np.asarray(x_train[0]) ).show()

x_train = x_train.reshape(60000, 784).astype("float32") / 255
x_test = x_test.reshape(10000, 784).astype("float32") / 255

model.compile(
        loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        optimizer=keras.optimizers.RMSprop(),
        metrics=["accuracy"])

if train:
    history = model.fit(x_train, y_train, batch_size=64, epochs=2,\
                        validation_split=0.2)

    test_scores = model.evaluate(x_test, y_test, verbose=2)
    print("Test loss:", test_scores[0])
    print("Test accuracy:", test_scores[1])

# we can also save our model
#model.save("./saved_model")

''' We can use the same graph of layers to define multiple models.
    We can also make models from models. '''
encoder_input = keras.Input(shape=(28, 28, 1), name='img')
x = layers.Conv2D(16, 3, activation='relu')(encoder_input)
x = layers.Conv2D(32, 3, activation='relu')(x)
x = layers.MaxPooling2D(3)(x)
x = layers.Conv2D(32, 3, activation='relu')(x)
x = layers.Conv2D(16, 3, activation='relu')(x)
encoder_output = layers.GlobalMaxPooling2D()(x)
encoder = keras.Model(encoder_input, encoder_output, name='encoder')
encoder.summary()

# conv2dtranspose is the reverse of conv2d and upsampling is reverse of maxpooling
#x = layers.Reshape((4, 4, 1))(encoder_output)
decoder_input = keras.Input(shape=(16,), name="encoded_img")
x = layers.Reshape((4, 4, 1))(decoder_input)
x = layers.Conv2DTranspose(16, 3, activation='relu')(x)
x = layers.Conv2DTranspose(32, 3, activation='relu')(x)
x = layers.UpSampling2D(3)(x)
x = layers.Conv2DTranspose(16, 3, activation='relu')(x)
decoder_output = layers.Conv2DTranspose(1, 3, activation='relu')(x)
decoder = keras.Model(decoder_input, decoder_output, name='decoder')

autoencoder_input = keras.Input(shape=(28, 28, 1), name='img')
encoded_img = encoder(autoencoder_input)
decoded_img = decoder(encoded_img)
autoencoder = keras.Model(autoencoder_input, decoded_img, name='autoencoder')
#autoencoder.summary()

''' example of model with multiple inputs and outputs, base off an example
    ranking system for ranking customer issues or something '''
num_tags = 12 # number of unique issue tags
num_words = 10000 # size of vocab obtained when preprocessing text
num_departments = 4

# variable length sequence of ints
title_input = keras.Input(shape=(None,), name="title")
body_input = keras.Input(shape=(None,), name='body')
# binary vectors of size 'num_tags'
tags_input = keras.Input(shape=(num_tags,), name='tags')

# embed each word into a 64 dimensional vector
title_features = layers.Embedding(num_words, 64)(title_input)
body_features = layers.Embedding(num_words, 64)(body_input)

# reduce sequence of embedded words into a single vector
title_features = layers.LSTM(128)(title_features)
body_features = layers.LSTM(32)(body_features)

# merge all features into a single large vector
x = layers.concatenate([title_features, body_features, tags_input])

# logistic regression for priority prediction on features
priority_pred = layers.Dense(1, name="priority")(x)
# department classifier on top of features
department_pred = layers.Dense(num_departments, name='departments')(x)

# instantiate an end-to-end model predicting both priority and department
model = keras.Model(
            inputs=[title_input, body_input, tags_input],
            outputs=[priority_pred, department_pred])

keras.utils.plot_model(model, "multi_input_output.png", show_shapes=True)

# assign different losses to each output
model.compile(
        optimizer=keras.optimizers.RMSprop(1e-3),
        loss={
            "priority": keras.losses.BinaryCrossentropy(from_logits=True),
            "departments": keras.losses.CategoricalCrossentropy(from_logits=True)
            },
        loss_weights=[1.0, 0.2],
    )

# Train model with dummy data
title_data = np.random.randint(num_words, size=(1280, 10))
body_data = np.random.randint(num_words, size=(1280, 10))
tags_data = np.random.randint(2, size=(1280, num_tags)).astype("float32")

priority_targets = np.random.random(size=(1280, 1))
dept_targets = np.random.randint(2, size=(1280, num_departments))

'''
model.fit({"title": title_data, "body": body_data, "tags": tags_data},
          {"priority": priority_targets, "departments": dept_targets},
          epochs=2,
          batch_size=32,
         )
'''
