import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from PIL import Image
import numpy as np

printImage = False

image_bytes = tf.io.read_file("./edge_detection_ex.jpeg")
img = tf.image.decode_image(image_bytes)
# display original image
if printImage:
    Image.fromarray( np.asarray(img), mode='RGB' ).show()

img = tf.image.rgb_to_grayscale(img)
img = tf.cast(img, tf.float32)

# let's duplicate and invert our image for the sake of it
imgs = tf.Variable([img, 255*tf.ones(img.shape, dtype=tf.float32) - img])

# imgs[1] is an EagerTensor object which then needs to be converted to an array
# we then take each channel separately.
#Image.fromarray( np.asarray(imgs[1, ..., 0]) ).show()
#Image.fromarray( np.asarray(imgs[1, ..., 1]) ).show()
#Image.fromarray( np.asarray(imgs[1, ..., 2]) ).show()

sobel = tf.image.sobel_edges(imgs)
sobel_y = np.asarray(sobel[0, :, :, 0])

#print( sobel_y[..., 0].shape )
#Image.fromarray( sobel_y[..., 0]).show()

""" sobel filter via convlution operations """
sobel_x = tf.constant([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype=tf.float32)
sobel_x_filter = tf.reshape(sobel_x, [3, 3, 1, 1])
sobel_y_filter = tf.transpose(sobel_x_filter, [1, 0, 2, 3])
#print("sobel_x: ", sobel_x_filter)
print("sobel_y: ", sobel_y_filter)

img = tf.expand_dims(img, 0)
print("img shape: ", img.shape)
filtered_y = tf.nn.conv2d(img, sobel_y_filter, strides=[1, 1, 1, 1], padding='SAME')
filtered_x = tf.nn.conv2d(img, sobel_x_filter, strides=[1, 1, 1, 1], padding='SAME')
print(filtered_y.shape)
print(filtered_x.shape)
if printImage:
    Image.fromarray( np.asarray(filtered_x[0, ..., 0] ) ).show()
    Image.fromarray( np.asarray(filtered_y[0, ..., 0] ) ).show()
