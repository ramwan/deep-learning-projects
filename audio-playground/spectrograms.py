import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf

from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras import layers
from tensorflow.keras import models

from IPython import display

data_dir = pathlib.Path("data/mini_speech_commands")
if not data_dir.exists():
    tf.keras.utils.get_file(
        'mini_speech_commands.zip',
        origin='http://storage.googleapis.com/download.tensorflow.org/data/mini_speech_commands.zip',
        extract=True,
        cache_dir='.', cache_subdir='data')

commands = np.array(tf.io.gfile.listdir(str(data_dir)))
commands = commands[commands != 'README.md']

# let's just use a set file for testing purposes
filenames = [b'data/mini_speech_commands/go/ffd2ba2f_nohash_3.wav']
#filenames = tf.io.gfile.glob(str(data_dir) + '/*/*')
#filenames = tf.random.shuffle(filenames)
num_samples = len(filenames)

def decode_audio(audio_binary):
    audio, _ = tf.audio.decode_wav(audio_binary)
    return tf.squeeze(audio, axis=1)

AUTOTUNE = tf.data.AUTOTUNE
# the tf.data.Dataset object just represents a potentially large set of elements
files_ds = tf.data.Dataset.from_tensor_slices(filenames)
''' If we want to view the dataset, we can print it like follows '''
#print( list(files_ds.as_numpy_iterator()) )
#waveform_ds = files_ds.map( ''' func ''' , num_parallel_calls=AUTOTUNE)

parts = tf.strings.split(filenames[0], os.path.sep)
label = parts[-2]
audio_binary = tf.io.read_file(filenames[0])
audio = decode_audio(audio_binary)

# pad for audio with less than 16000 samples
zero_padding = tf.zeros([16000] - tf.shape(audio), dtype=tf.float32)
print(filenames[0])
print(zero_padding.shape)
print(audio.shape)

waveform = tf.cast(audio, tf.float32)
padded_waveform = tf.concat([waveform, zero_padding], 0)
hann_spectrogram = tf.signal.stft(padded_waveform,
                                  frame_length=255,
                                  frame_step=128,
                                  window_fn=tf.signal.hann_window)
hann_spectrogram = tf.abs(hann_spectrogram)

fig, ax = plt.subplots(5)
time_scale = np.arange(waveform.shape[0])
ax[0].plot(time_scale, waveform.numpy())

log_spec = np.log(hann_spectrogram.numpy().T)
height = log_spec.shape[0]
width = log_spec.shape[1]
X = np.linspace(0, np.size(hann_spectrogram), num=width, dtype=int)
Y = range(height)
ax[1].pcolormesh(X, Y, log_spec, shading='auto')

hamming_spectrogram = tf.signal.stft(padded_waveform,
                                     frame_length=255,
                                     frame_step=128,
                                     window_fn=tf.signal.hamming_window)
hamming_spectrogram = tf.abs(hamming_spectrogram)

log_spec = np.log(hamming_spectrogram.numpy().T)
height = log_spec.shape[0]
width = log_spec.shape[1]
X = np.linspace(0, np.size(hamming_spectrogram), num=width, dtype=int)
Y = range(height)
ax[2].pcolormesh(X, Y, log_spec, shading='auto')

diff_spectrogram = tf.abs(hamming_spectrogram - hann_spectrogram)
log_spec = np.log(diff_spectrogram.numpy().T)
height = log_spec.shape[0]
width = log_spec.shape[1]
X = np.linspace(0, np.size(diff_spectrogram), num=width, dtype=int)
Y = range(height)
ax[3].pcolormesh(X, Y, log_spec, shading='auto')

spectrogram = tf.signal.stft(padded_waveform,
                             frame_length=255,
                             frame_step=128,
                             window_fn=None)
spectrogram = tf.abs(spectrogram)

log_spec = np.log(spectrogram.numpy().T)
height = log_spec.shape[0]
width = log_spec.shape[1]
X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
Y = range(height)
ax[4].pcolormesh(X, Y, log_spec, shading='auto')
plt.show()
