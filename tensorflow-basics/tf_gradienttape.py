import tensorflow as tf

x = tf.Variable(3.0)
with tf.GradientTape() as tape:
    y = x**2

print("we don't need to be in the same context?...")
# dy = 2x * dx
dy_dx = tape.gradient(y, x)
print(dy_dx.numpy())

w = tf.Variable(tf.random.normal((3, 2)), dtype=tf.float32, name='w')
b = tf.Variable(tf.zeros(2, dtype=tf.float32), name='b')
x = [[1., 2., 3.]]
with tf.GradientTape(persistent=True) as tape:
    y = x@w + b
    loss = tf.reduce_mean(y**2)
[dl_dw, dl_db] = tape.gradient(loss, [w, b])
print(dl_dw)
print(dl_db)

myVars = {'w':w, 'b':b}
grad = tape.gradient(loss, myVars)
print(grad['b'])

layer = tf.keras.layers.Dense(2, activation='relu')
x = tf.constant([[1., 2., 3.]])
with tf.GradientTape() as tape:
    # forward pass
    y = layer(x)
    loss = tf.reduce_mean(y**2)
grad = tape.gradient(loss, layer.trainable_variables)
print(grad)

# oh what.... the variable for a with statement persists but it is essentially
# ended and unused - eg. the file will have been closed but we still have
# the file handle object
#with open("./tf_gradienttape.py", 'r') as f:
#    print("within with context")
#    b = 2
#print(b)
#print(f.read(1))
