import tensorflow as tf
import timeit
from datetime import datetime

def a_regular_function(x, y, b):
    x = tf.matmul(x, y)
    x = x + b
    return x

a_function_that_uses_a_graph = tf.function(a_regular_function)
x1 = tf.constant([[1., 2.]])
y1 = tf.constant([[2.], [3.]])
b1 = tf.constant(4.)

orig_value = a_regular_function(x1, y1, b1).numpy()
tf_function_value = a_function_that_uses_a_graph(x1, y1, b1).numpy()
assert(orig_value == tf_function_value)

""" @tf.function applies to all nested functions """
def inner_function(x, y, b):
    x = tf.matmul(x, y)
    x = x +b
    return x

@tf.function
def outer_function(x):
    y = tf.constant([[2.], [3.]])
    b = tf.constant(4.)

    return inner_function(x, y, b)

""" converting python functions to graphs """
def simple_relu(x):
    if tf.greater(x, 0):
        return x
    else:
        return 0

tf_simple_relu = tf.function(simple_relu)
print("First branch, with graph:", tf_simple_relu(tf.constant(1)).numpy())
print("Second branch with graph:", tf_simple_relu(tf.constant(-1)).numpy())
#print(tf.autograph.to_code(simple_relu))

# each call to this function with a different signature creates a new graph
#@tf.function
#def my_relu(x):
#    return tf.maximum(0., x)
#
#my_relu(tf.constants(5.5))
#my_relu([1, -1])
#my_relu(tf.constants([3., -3.])

@tf.function
def get_MSE(y_true, y_pred):
    print("Calculating MSE!")
    sq_diff = tf.pow(y_true - y_pred, 2)
    return tf.reduce_mean(sq_diff)

y_true = tf.random.uniform([5], maxval=10, dtype=tf.int32)
y_pred = tf.random.uniform([5], maxval=10, dtype=tf.int32)

tf.config.run_functions_eagerly(False)

get_MSE(y_true, y_pred)
get_MSE(y_true, y_pred)

