import tensorflow as tf

class SimpleModule(tf.Module):
    def __init__(self, name=None):
        super().__init__(name=name)
        self.a_variable = tf.Variable(5.0, name="train me")
        self.non_trainable_variable = tf.Variable(5.0, trainable=False, name="dont train me")
    def __call__(self, x):
        return self.a_variable * x + self.non_trainable_variable

simple_module = SimpleModule(name="simple")
simple_module(tf.constant(5.0))
print(simple_module.trainable_variables)
print(simple_module.variables)

# example of dense linear layer
class Dense(tf.Module):
    def __init__(self, in_features, out_features, name=None):
        super().__init__(name=name)
        self.w = tf.Variable(\
                tf.random.normal([in_features, out_features], name='w'))
        self.b = tf.Variable(tf.zeros([out_features]), name='b')
    def __call__(self, x):
        y = tf.matmul(x, self.w) + self.b
        return tf.nn.relu(y)

class SequentialModule(tf.Module):
    def __init__(self, name=None):
        super().__init__(name=name)

        self.dense_1 = Dense(in_features=3, out_features=3)
        self.dense_2 = Dense(in_features=3, out_features=2)

    @tf.function
    def __call__(self, x):
        x = self.dense_1(x)
        return self.dense_2(x)

my_model = SequentialModule(name="the_model")
#print("Model Results:", my_model(tf.constant([[2., 0., 2.]])))
#for var in my_model.variables:
#    print(var)

logdir = "./logs/sample"
writer = tf.summary.create_file_writer(logdir)
tf.summary.trace_on(graph=True)
tf.profiler.experimental.start(logdir)
print(my_model(tf.constant([[2., 0., 2.]])))
with writer.as_default():
    tf.summary.trace_export(\
            name="my_func_trace",\
            step=0,\
            profiler_outdir=logdir)
