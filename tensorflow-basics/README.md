Following sample tensorflow tutorial for mnist dataset with tensorboard logging added.

- we note that RNN training is super slow compared to the others.

For the fashion\_mnist dataset, we note that
- Adam gives test accuracy of 85%
- Adagrad gives test accuracy of 69%
- RMSprop gives test accuracy of 85%
- SGD gives test accuracy of 83%
- we note that adagrad accuracy starts of at sub50 but the others start above 50
- for such a small and fast dataset we don't note any difference in processing times
- SGD starts between Adam/RMSprop and Adagrad and gives results in between the 2
- all with default parameters

- We note that adagrad adjusts the gradient by scaling the calculated gradient with 
inverse square of summed gradients from the start of training and this shows when 
we run the training with 4x more epochs and the increase in test accuracy drops off 
very rapidly, progressing at less than 1% per epoch.
- In the fashion\_mnist example we note that rmsprop and adam both have essentially 
the same result.
