import tensorflow as tf

#tf.debugging.set_log_device_placement(True)

v1 = tf.Variable(0)
v2 = tf.Variable(0)

# we note that tensorflow evaluates values immediately now rather than storing
# it as a ?global graph operation?
for i in range(10):
    v1.assign(v2 + 1)
    v2.assign(v1 + 1)

    print(f"v1: {v1.read_value()}")
    print(f"v2: {v2.read_value()}")

# sample MLP
def dense(x, W, b):
    return tf.nn.sigmoid(tf.matmul(x, W) + b)

@tf.function
def MLP(x, w0, b0, w1, b1):
    x = dense(x, w0, b0)
    x = dense(x, w1, b1)

# the above is the same as
#hidden_size = 4 #placeholder
#layers = [tf.keras.layers.Dense(hidden_size, activation=tf.nn.sigmoid) for _ in range(n)]
#perceptrom = tf.keras.Sequential(layers)

# another example of things that depend on tensor variables running in
# sequential order
def fizzbuzz(max_num):
    counter = tf.constant(0)
    max_num = tf.convert_to_tensor(max_num)
    for num in range(1, max_num.numpy() + 1):
        num = tf.constant(num)
        if int(num % 3) == 0 and int(num % 5) == 0:
            print('FizzBuzz')
        elif int(num % 3) == 0:
            print('Fizz')
        elif int(num % 5) == 0:
            print('Buzz')
        else:
            print(num.numpy())
        counter += 1

fizzbuzz(15)

# generating sample distributions and custom models
class Linear(tf.keras.Model):
    def __init__(self):
        super(Linear, self).__init__()
        self.W = tf.Variable(5., name='weight')
        self.B = tf.Variable(10., name='bias')
    def call(self, inputs):
        return inputs * self.W + self.B

NUM_EXAMPLES = 2000
training_inputs = tf.random.normal([NUM_EXAMPLES])
noise = tf.random.normal([NUM_EXAMPLES])
training_outputs = training_inputs * 3 + 2 + noise

def loss(model, inputs, targets):
    error = model(inputs) - targets
    return tf.reduce_mean(tf.square(error))

def grad(model, inputs, targets):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, targets)
    return tape.gradient(loss_value, [model.W, model.B])

model = Linear()
optimizer = tf.keras.optimizers.SGD()
print(f"Initial loss: {loss(model, training_inputs, training_outputs)}")

steps = 300
for i in range(steps):
    grads = grad(model, training_inputs, training_outputs)
    optimizer.apply_gradients(zip(grads, [model.W, model.B]))
    if i % 20 == 0:
        print(f"Loss at step {i}: {loss(model, training_inputs, training_outputs)}")
