import tensorflow as tf
from tensorflow import keras
import tensorboard

model = keras.models.Sequential([
            keras.layers.Flatten(input_shape=(28,28)),
            keras.layers.Dense(32, activation='relu'),
            keras.layers.Dropout(0.2),
            keras.layers.Dense(10, activation='softmax')
        ])

optim = 'sgd'
model.compile(
        optimizer=optim,
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy'])

(train_images, train_labels), _ = keras.datasets.fashion_mnist.load_data()
train_images = train_images / 255.0 # normalise

logdir = "./logs/mnist_fashion_" + optim
tensorboard_cb = keras.callbacks.TensorBoard(log_dir=logdir)
model.fit(train_images, train_labels, batch_size=64, epochs=10, callbacks=[tensorboard_cb])

